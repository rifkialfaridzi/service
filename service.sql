-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2022 at 08:41 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` int(11) NOT NULL,
  `transaksi` int(11) NOT NULL,
  `item` varchar(30) NOT NULL,
  `harga` varchar(30) NOT NULL,
  `kondisi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id`, `transaksi`, `item`, `harga`, `kondisi`) VALUES
(1, 4, 'Aki Yoshinoya', '13200', 'baru'),
(3, 4, 'Jasa Pasang', '10000', '');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(10) NOT NULL,
  `user` int(11) NOT NULL,
  `nama_kendaraan` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL,
  `tahun` int(5) NOT NULL,
  `nopol` varchar(10) NOT NULL,
  `kerusakan` text NOT NULL,
  `solusi` text NOT NULL,
  `kelengkapan` text NOT NULL,
  `penerima` int(11) NOT NULL,
  `tanggal_terima` date NOT NULL,
  `tanggal_ambil` date NOT NULL,
  `status` int(1) NOT NULL,
  `biaya` int(10) NOT NULL,
  `kondisi_barang` enum('Baru','Second') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_transaksi`, `user`, `nama_kendaraan`, `warna`, `tahun`, `nopol`, `kerusakan`, `solusi`, `kelengkapan`, `penerima`, `tanggal_terima`, `tanggal_ambil`, `status`, `biaya`, `kondisi_barang`) VALUES
(2, 'TRX0001', 48, 'Honda HRV', 'Hitam', 2021, 'AD 3894 IK', '<p>Tidak Bisa Di starter    </p>', '<p><br></p>', '<p>Full Set</p>', 45, '2022-06-10', '2022-06-20', 5, 0, ''),
(3, 'TRX0002', 48, 'Honda CRV', 'Ungu', 2017, 'AD 5732 AS', '<p>Mesin Mogok</p>', '<p>Ganti Aki</p>', '<p>Full Set</p>', 45, '2022-06-10', '0000-00-00', 5, 250000, 'Second'),
(4, 'TRX0003', 48, 'Honda CRV', 'Hitam', 2021, 'AD 5732 AS', '<p>Tidak Mau di starter</p>', '<p>Ganti Oli</p>', '<p>Lengkap</p>', 45, '2022-07-10', '0000-00-00', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `created_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2020-03-21'),
(45, 'teknisi', '21232f297a57a5a743894a0e4a801fc3', 2, '2022-04-30'),
(48, '6285156230063', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2022-05-15'),
(50, 'MheXs', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2022-06-08'),
(51, '081373833838', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2022-06-08');

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nomor_telepon` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`user`, `nama`, `nomor_telepon`, `alamat`, `tanggal_masuk`, `foto`) VALUES
(44, 'Bambang Pamungkasa', '6285 1562 3007', '<p>asasa</p>', '2022-05-17', ''),
(45, 'Rifki Alfaridzi', '6285 1562 3005', '<p>asas</p>', '2021-04-30', ''),
(48, 'Paulos', '62 8515 6230 063', '<p>asas</p>', '2022-05-15', 'LKxilnW3kj.jpg'),
(50, 'Wiratama Desigs', '0813 8373 3736', '<p>Solo Ngalor Sitiks</p>', '2022-06-08', 'wcK9uxlkP3.jpg'),
(51, 'Hendro Yuwono', '0813 7383 3838', '<p>Joyotakan Njojrok sitik</p>', '2022-06-08', 'tVZ7FajH6U.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD UNIQUE KEY `user` (`user`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
