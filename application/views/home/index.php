<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SERVICE</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url('assets/template/'); ?>assets/img/favicon.png" rel="icon">
  <link href="<?php echo base_url('assets/template/'); ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/template/'); ?>assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url('assets/template/'); ?>assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.8.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html">SISTEM INFORMASI BENGKEL</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
     

      <nav id="navbar" class="navbar">
        
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
   <!-- ======= Hero Section ======= -->
   <section id="hero" style="background-image: url('<?php echo base_url('assets/template/'); ?>assets/img/bg2.png');background-size: cover;background-blend-mode: multiply;background-blend-mode: multiply;
  background-color: #666666;" class="d-flex align-items-center min-vh-100">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Bengkel Slamet</h1>
          <p style="color: white;">Bengkel Mobil Slamet Makam Haji Sukoharjo adalah penyedia jasa yang memiliki spesialisasi dalam hal perawatan dan perbaikan salah satu elemen mobil. Sebagai contoh bengkel body repair, radiator, AC, spooring, balancing, dan sebagainya. Spesialisasi yang dilakukan oleh bengkel tersebut menuntut pelayanan khusus sesuai dengan jenis operasi yang akan dilakukan.</p>
          <h3 style="color: white;">Bengkel Slamet - Jalan Parang Barong Kec. Kartasura - 0852-0127-4565</h3>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a style="background-color: red;" href="<?php echo base_url('auth'); ?>" class="btn-get-started">Masuk</a>
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a> -->
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="<?php echo base_url(); ?>/assets/img/a1a.jpg" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->


  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('assets/template/'); ?>assets/js/main.js"></script>

</body>

</html>