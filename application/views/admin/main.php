<section ng-app="myApp" ng-controller="someController" class="section">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-users"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Jumlah Pengguna</h4>
          </div>
          <div class="card-body">
            <?php echo count($pengguna); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-users"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Jumlah Transaksi</h4>
          </div>
          <div class="card-body">
            <?php echo count($service); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Transaksi Terakhir</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>Kode Service</th>
                <th>Customer</th>
                <th>Kendaraan</th>
                <th>Warna</th>
                <th>Nomor Polisi</th>
                <th>Tanggal</th>
                <th>Status</th>
              </tr>
              <?php $i = 1;

              foreach ($service as $key) {

              ?>
                <tr>
                  <td> <?php echo $key->kode_transaksi; ?></td>
                  <td> <?php echo $key->nama_customer; ?></td>
                  <td> <?php echo $key->nama_kendaraan; ?></td>
                  <td> <?php echo $key->warna; ?></td>
                  <td> <?php echo $key->nopol; ?></td>
                  <td> <?php echo $key->tanggal_terima; ?></td>
                  <td> 
                  <?php 

                  switch ($key->status) {
                    case 0:
                        $pesan = '<div class="badge badge-info">Penerimaan Kendaraan</div>';
                        break;
                    case 1:
                        $pesan = '<div class="badge badge-info">Pengecekan Kendaraan</div>';
                        break;
                    case 2:
                        $pesan = '<div class="badge badge-success">Penggantian Sparepart Disetujui</div>';
                        break;
                    case 3:
                        $pesan = '<div class="badge badge-success">Proses Pemasangan Sparepart</div>';
                        break;
                    case 4:
                        $pesan = '<div class="badge badge-success">Bisa Di Ambil</div>';
                        break;
                    case 5:
                        $pesan = '<div class="badge badge-success">Transaksi Selesai</div>';
                        break;
                    case 6:
                        $pesan = '<div class="badge badge-danger">Transaksi Di Batalkan</div>';
                        break;
                    default:
                        # code...
                        break;
                }
                  echo $pesan;
                  ?>

                  </td>
                 
                </tr>

              <?php if (++$i > 4) break;
              } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  var app = angular.module('myApp', ['ui.bootstrap']);

  app.controller('someController', function($scope, $filter, $http) {



    $scope.selectMonth = function(month) {
      $http.get('<?php echo base_url("admin/dashboard/"); ?>' + month, {
        msg: 'hello word!'
      }).
      then(function(response) {
        $scope.totalSupplier = response.data.supplier;
        $scope.totalOrder = response.data.totalOrder;
        $scope.totalProdukOrder = response.data.totalProdukOrder;
        $scope.totalPendapatan = response.data.totalPendapatan;
        $scope.topProduk = response.data.topProduk;
        $scope.totalProduk = response.data.produk.jumlah_produk;

        console.log(response.data);
      }, function(response) {
        console.log('error bos');
      });

    }

    var date = new Date();
    $scope.selectMonth(date.getMonth() + 1);





  });
</script>