<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Customer Baru</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("pengguna/create_action"); ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
								<div class="col-sm-12 col-md-7">
									<div id="image-preview" class="image-preview">
										<label for="image-upload" id="image-label">Pilih Gambar</label>
										<input type="file" name="foto" id="image-upload" accept="image/png, image/jpeg, image/jpg, image/gif" />
									</div>
									<div class="invalid-feedback">
										Foto Customer Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="nama" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama Customer Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Telepon</label>
								<div class="col-sm-12 col-md-7">
									<input id="nik" type="text" class="form-control phone-numbers" name="nomor_telepon" required autofocus>
									<div class="invalid-feedback">
										Nomor Telepon Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="alamat" class="summernote"></textarea>
									<div class="invalid-feedback">
										Alamat Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambahkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>

	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' ', ' '],
		blocks: [2, 4, 4, 4]
	});

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>