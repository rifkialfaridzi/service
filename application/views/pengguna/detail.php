<section class="section">
	<div class="section-header">
		<h1>Halaman Service</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Service</h4>
						<div class="card-header-action">
							<a href="<?php echo base_url('service/create'); ?>" class="btn btn-primary">
								<i class="fa fa-plus"></i> Tambah Service
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tabel_service" class="table table-striped">
								<thead>
									<tr>
										<th>Kode Service</th>
										<th>Biaya</th>
										<th>Kendaraan</th>
										<th>Warna</th>
										<th>Nomor Polisi</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Service</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#tabel_service').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pengguna/data_service_by_pengguna/').$id_user; ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "kode_transaksi"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return "Rp. "+row.total_biaya.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
					}
				},
				{
					"data": "nama_kendaraan"
				},
				{
					"data": "warna"
				},
				{
					"data": "nopol"
				},
				{
					"data": "tanggal_terima"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						switch (parseInt(row.status)) {
							case 0:
								return '<div class="badge badge-info">Penerimaan Kendaraan</div>';
								break;
							case 1:
								return '<div class="badge badge-info">Pengecekan Kendaraan</div>';
								break;
							case 2:
								return '<div class="badge badge-success">Penggantian Sparepart Disetujui</div>';
								break;
							case 3:
								return '<div class="badge badge-success">Proses Pemasangan Sparepart</div>';
								break;
							case 4:
								return '<div class="badge badge-success">Bisa Di Ambil</div>';
								break;
							case 5:
								return '<div class="badge badge-success">Transaksi Selesai</div>';
								break;
							case 6:
								return '<div class="badge badge-danger">Transaksi Di Batalkan</div>';
								break;
							default:
								break;
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {

						if (row.status != 5) {
							return '<a href="<?php echo site_url("service/send_wa/") ?>' + row.id + '" target="_blank" class="btn btn-icon btn-success"><i class="fab fa-whatsapp"></i></a> <a href="<?php echo site_url("service/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						}else{
							return '<a href="<?php echo site_url("service/send_wa/") ?>' + row.id + '" target="_blank" class="btn btn-icon btn-success"><i class="fab fa-whatsapp"></i></a> <a href="<?php echo site_url("service/print_nota/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fa fa-print"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						}

						
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("service/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>