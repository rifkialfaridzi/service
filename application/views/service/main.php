<section class="section">
	<div class="section-header">
		<h1>Halaman Service</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Service</h4>
						<div class="card-header-action">
							<a href="<?php echo base_url('service/create'); ?>" class="btn btn-primary">
								<i class="fa fa-plus"></i> Tambah Service
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Pilih Data</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<i class="fas fa-calendar"></i>
									</div>
								</div>
								<select name="periode" id="periode" class="form-control select2">
									<option value="1">Januari</option>
									<option value="2">Februari</option>
									<option value="3">Maret</option>
									<option value="4">April</option>
									<option value="5">Mei</option>
									<option value="6">Juni</option>
									<option value="7">Juli</option>
									<option value="8">Agustus</option>
									<option value="9">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
								<div class="input-group-prepend">
									<select name="status" id="status" class="form-control select2">
										<option value="all">Tampilkan Semua Data</option>
										<option value="0">Penerimaan Kendaraan</option>
										<option value="1">Pengecekan Kendaraan</option>
										<option value="2">Penggantian Sparepart Disetujui</option>
										<option value="3">Proses Pemasangan Sparepart</option>
										<option value="4">Bisa Di Ambil</option>
										<option value="5">Transaksi Selesai</option>
										<option value="6">Transaksi Di Batalkan</option>
									</select>
								</div>
								<div class="input-group-prepend">
									<div class="input-group-text">
										<a id="cetak" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table id="tabel_service" class="table table-striped">
								<thead>
									<tr>
										<th>Kode Service</th>
										<th>Customer</th>
										<th>Kendaraan</th>
										<th>Warna</th>
										<th>Nomor Polisi</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Service</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#tabel_service').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('service/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "kode_transaksi"
				},
				{
					"data": "nama_customer"
				},
				{
					"data": "nama_kendaraan"
				},
				{
					"data": "warna"
				},
				{
					"data": "nopol"
				},
				{
					"data": "tanggal_terima"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						switch (parseInt(row.status)) {
							case 0:
								return '<div class="badge badge-info">Penerimaan Kendaraan</div>';
								break;
							case 1:
								return '<div class="badge badge-info">Pengecekan Kendaraan</div>';
								break;
							case 2:
								return '<div class="badge badge-success">Penggantian Sparepart Disetujui</div>';
								break;
							case 3:
								return '<div class="badge badge-success">Proses Pemasangan Sparepart</div>';
								break;
							case 4:
								return '<div class="badge badge-success">Bisa Di Ambil</div>';
								break;
							case 5:
								return '<div class="badge badge-success">Transaksi Selesai</div>';
								break;
							case 6:
								return '<div class="badge badge-danger">Transaksi Di Batalkan</div>';
								break;
							default:
								break;
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {

						if (row.status != 5) {
							return '<a href="<?php echo site_url("service/send_wa/") ?>' + row.id + '" target="_blank" class="btn btn-icon btn-success"><i class="fab fa-whatsapp"></i></a> <a href="<?php echo site_url("service/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						} else {
							return '<a href="<?php echo site_url("service/send_wa/") ?>' + row.id + '" target="_blank" class="btn btn-icon btn-success"><i class="fab fa-whatsapp"></i></a> <a href="<?php echo site_url("service/print_nota/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fa fa-print"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						}


					}
				}
			],

		});



	});

	$('#periode').on('change', function(e) {
		table.ajax.url("<?php echo site_url('service/short_data_service?periode=') ?>"+this.value + "&status=" + $("#status").val()).load();
	});

	$('#status').on('change', function(e) {
		table.ajax.url("<?php echo site_url('service/short_data_service?periode=') ?>"+$("#periode").val()+ "&status=" + this.value).load();
	});

	$("#cetak").click(function() {
            window.open("<?php echo site_url('service/laporan_service?periode=') ?>"+$("#periode").val()+ "&status=" + $("#status").val());
            // console.log(getFirstDate);
            // console.log(getLastDate);
        });

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("service/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>