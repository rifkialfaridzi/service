<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Ubah Data Service</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah Data</h4>
						<div class="card-header-action">
							<a href="<?php echo base_url('service/print_nota/') . $data_service[0]->id; ?>" class="btn btn-success">
								<i class="fa fa-print"></i> Cetak Nota
							</a>
						</div>
					</div>
					<div class="card-body">
						<form method="POST" action="<?php echo base_url("service/update_action/") . $data_service[0]->id; ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Customer</label>
								<div class="col-sm-12 col-md-7">
									<select name="user" class="form-control select2" disabled>
										<option value="">Pilih Customer</option>
										<?php foreach ($customer as $key) { ?>
											<option <?php echo $key->user == $data_service[0]->user ? "selected" : ""; ?> value="<?php echo $key->user; ?>"><?php echo $key->nama; ?> | <?php echo $key->nomor_telepon; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kendaraan</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" value="<?php echo $data_service[0]->nama_kendaraan; ?>" name="nama_kendaraan" tabindex="1" placeholder="Suzuki Ertiga" required autofocus>
									<div class="invalid-feedback">
										Nama Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Warna</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" value="<?php echo $data_service[0]->warna; ?>" name="warna" tabindex="1" placeholder="Merah" required autofocus>
									<div class="invalid-feedback">
										Warna Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tahun</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" value="<?php echo $data_service[0]->tahun; ?>" name="tahun" tabindex="1" placeholder="2022" required autofocus>
									<div class="invalid-feedback">
										Tahun Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Polisi</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control nopol" value="<?php echo $data_service[0]->nopol; ?>" name="nopol" placeholder="AD 3452 IH" required autofocus>
									<div class="invalid-feedback">
										Nomor Polisi Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelengkapan</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="kelengkapan" class="summernote"><?php echo $data_service[0]->kelengkapan; ?></textarea>
									<div class="invalid-feedback">
										Kelengkapan Masih Kosong
									</div>
									<small>Diskripsikan Kelengkapan Kendaraan</small>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kerusakan</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="kerusakan" class="summernote"><?php echo $data_service[0]->kerusakan; ?></textarea>
									<div class="invalid-feedback">
										Kerusakan Masih Kosong
									</div>
									<small>Diskripsikan Kerusakan Kendaraan</small>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Solusi</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="solusi" class="summernote"><?php echo $data_service[0]->solusi; ?></textarea>
									<div class="invalid-feedback">
										Solusi Masih Kosong
									</div>
									<small>Diskripsikan Solusi Kerusakan Kendaraan</small>
								</div>
							</div>
							<!-- <div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Biaya</label>
								<div class="col-sm-12 col-md-7">
									<div class="input-group">
										<input type="text" class="form-control nopol" value="<?php //echo $data_service[0]->biaya; 
																								?>" name="biaya" placeholder="AD 3452 IH" required autofocus>
										<select class="custom-select" name="kondisi_barang" id="inputGroupSelect05">
											<option selected="">Pilih Kondisi Sparepart</option>
											<option <?php //echo $data_service[0]->kondisi_barang == "Baru" ? "selected" : ""; 
													?> value="Baru">Baru</option>
											<option <?php //echo $data_service[0]->kondisi_barang == "Second" ? "selected" : ""; 
													?> value="Second">Second</option>
										</select>
									</div>

									<div class="invalid-feedback">
										Biaya Masih Kosong
									</div>
								</div>
							</div> -->
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status Transaksi</label>
								<div class="col-sm-12 col-md-7">
									<select name="status" class="form-control select2">

										<option <?php echo $data_service[0]->status == "0" ? "selected" : "" ?> value="0">Penerimaan Kendaraan</option>
										<option <?php echo $data_service[0]->status == "1" ? "selected" : "" ?> value="1">Pengecekan Kendaraan</option>
										<option <?php echo $data_service[0]->status == "2" ? "selected" : "" ?> value="2">Penggantian Sparepart Disetujui</option>
										<option <?php echo $data_service[0]->status == "3" ? "selected" : "" ?> value="3">Proses Pemasangan Sparepart</option>
										<option <?php echo $data_service[0]->status == "4" ? "selected" : "" ?> value="4">Bisa Di Ambil</option>
										<option <?php echo $data_service[0]->status == "5" ? "selected" : "" ?> value="5">Transaksi Selesai</option>
										<option <?php echo $data_service[0]->status == "6" ? "selected" : "" ?> value="6">Transaksi Di Batalkan</option>

									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Ubah</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Item Nota</h4>
						<div class="card-header-action">
						</div>
					</div>
					<div class="card-body">
						<form method="POST" action="<?php echo base_url("service/add_item_action/") . $data_service[0]->id; ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">

							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Produk/Jasa</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="item" tabindex="1" placeholder="Aki Yosinoya" required autofocus>
									<div class="invalid-feedback">
										Nama Produk/Jasa Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="number" class="form-control" name="harga" tabindex="1" placeholder="100000" required autofocus>
									<div class="invalid-feedback">
										Harga Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kondisi</label>
								<div class="col-sm-12 col-md-7">
									<select name="kondisi" class="form-control select2">
										<option value="">Pilih Kondisi</option>
										<option value="baru">Baru</option>
										<option value="second">Second</option>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambah</button>
								</div>
							</div>
						</form>

						<div class="table-responsive">
							<table id="category_tabels" class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Produk/Jasa</th>
										<th>Harga</th>
										<th>Kondisi</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $nomor = 1;
									$totalBayar = 0;
									foreach ($detail_service as $key) {
										$totalBayar +=  intval($key->harga); ?>
										<tr>
											<td><?php echo $nomor++; ?></td>
											<td><?php echo $key->item; ?></td>
											<td><?php echo "Rp. " . number_format($key->harga, 0, ',', '.'); ?></td>
											<td><?php echo $key->kondisi; ?></td>
											<td><a href="<?php echo base_url('service/delete_item/') . $data_service[0]->id . "?item=" . $key->id; ?>" class="btn btn-icon btn-danger"><i class="fas fa-times"></i></a></td>
										</tr>
									<?php } ?>
									<tr>
										<td colspan="2" style="text-align: center;font-weight: bold;">Total Bayar</td>
										<td colspan="3" style="center;font-weight: bold;"><?php echo "Rp. " . number_format($totalBayar, 0, ',', '.'); ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Order Status</h4>
					</div>
					<div class="card-body">
						<div class="row mt-4">
							<div class="col-12 col-lg-8 offset-lg-2">
								<div class="wizard-steps">
									<?php $status_label1 = "";
									if (intval($data_service[0]->status) >= 0 && intval($data_service[0]->status) <= 6) { ?>
										<div class="wizard-step wizard-step-active">
											<div class="wizard-step-icon">
												<i class="fas fa-shipping-fast"></i>
											</div>
											<div class="wizard-step-label">
												<?php switch (intval($data_service[0]->status)) {
													case 0:
														echo "Penerimaan Kendaraan";
														break;
													case 1:
														echo "Pengecekan kendaraan";
														break;
													case 2:
														echo "Penggantian Sparepart Disetujui";
														break;
													default:
														echo "Penggantian Sparepart Disetujui";
														break;
												} ?>
											</div>
										</div>
									<?php } else { ?>
										<div class="wizard-step">
											<div class="wizard-step-icon">
												<i class="fas fa-shipping-fast"></i>
											</div>
											<div class="wizard-step-label">
												<?php switch (intval($data_service[0]->status)) {
													case 0:
														echo "Penerimaan Kendaraan";
														break;
													case 1:
														echo "Pengecekan kendaraan";
														break;
													case 2:
														echo "Penggantian Sparepart Disetujui";
														break;
													default:
														echo "Penggantian Sparepart Disetujui";
														break;
												} ?>
											</div>
										</div>
									<?php } ?>

									<?php $status_label2 = "";
									if (intval($data_service[0]->status) >= 3 && intval($data_service[0]->status) <= 6) { ?>
										<div class="wizard-step wizard-step-active">
											<div class="wizard-step-icon">
												<i class="fas fa-car-battery"></i>
											</div>
											<div class="wizard-step-label">
												Proses Pemasangan Sparepart
											</div>
										</div>
									<?php } else { ?>
										<div class="wizard-step">
											<div class="wizard-step-icon">
												<i class="fas fa-car-battery"></i>
											</div>
											<div class="wizard-step-label">
												Proses Pemasangan Sparepart
											</div>
										</div>
									<?php } ?>

									<?php $status_label2 = "";
									if (intval($data_service[0]->status) >= 4 && intval($data_service[0]->status) <= 6) { ?>
										<div class="wizard-step wizard-step-active">
											<div class="wizard-step-icon">
												<i class="fas fa-car"></i>
											</div>
											<div class="wizard-step-label">
												Bisa Diambil
											</div>
										</div>
									<?php } else { ?>
										<div class="wizard-step">
											<div class="wizard-step-icon">
												<i class="fas fa-car"></i>
											</div>
											<div class="wizard-step-label">
												Bisa Diambil
											</div>
										</div>
									<?php } ?>

									<?php $status_label2 = "";
									if (intval($data_service[0]->status) >= 5 && intval($data_service[0]->status) <= 6) { ?>
										<div class="wizard-step <?php if (intval($data_service[0]->status == 5)) {
																	echo "wizard-step-success";
																} elseif (intval($data_service[0]->status == 6)) {
																	echo "wizard-step-danger";
																} ?>">
											<div class="wizard-step-icon">

												<?php switch (intval($data_service[0]->status)) {
													case 5:
														echo "<i class='fas fa-check'></i>";
														break;
													case 6:
														echo "<i class='fas fa-times-circle'></i>";
														break;
													default:
														echo "<i class='fas fa-check'></i>";
														break;
												} ?>

											</div>
											<div class="wizard-step-label">
												<?php switch (intval($data_service[0]->status)) {
													case 5:
														echo "Transaksi Selesai";
														break;
													case 6:
														echo "Transaksi Dibatalkan";
														break;
													default:
														echo "Transaksi Selesai";
														break;
												} ?>

											</div>
										</div>
									<?php } else { ?>
										<div class="wizard-step">
											<div class="wizard-step-icon">
											<?php switch (intval($data_service[0]->status)) {
													case 5:
														echo "<i class='fas fa-check'></i>";
														break;
													case 6:
														echo "<i class='fas fa-times-circle'></i>";
														break;
													default:
														echo "<i class='fas fa-check'></i>";
														break;
												} ?>

											</div>
											<div class="wizard-step-label">
											<?php switch (intval($data_service[0]->status)) {
													case 5:
														echo "Transaksi Selesai";
														break;
													case 6:
														echo "Transaksi Dibatalkan";
														break;
													default:
														echo "Transaksi Selesai";
														break;
												} ?>

											</div>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>
	var cleave = new Cleave(' .nopol', {
		numericOnly: false,
		delimiters: [' ', ' ', ' '],
		blocks: [2, 4, 4]
	});

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>