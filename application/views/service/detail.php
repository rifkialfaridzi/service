<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Detail Service</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("service/create_action"); ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">

							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Customer</label>
								<div class="col-sm-12 col-md-7">
									<select name="user" class="form-control select2">
										<option value="">Pilih Customer</option>
										<?php foreach ($customer as $key) { ?>
										<option value="<?php echo $key->user; ?>"><?php echo $key->nama; ?> | <?php echo $key->nomor_telepon; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kendaraan</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="nama_kendaraan" tabindex="1" placeholder="Suzuki Ertiga" required autofocus>
									<div class="invalid-feedback">
										Nama Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Warna</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="warna" tabindex="1" placeholder="Merah" required autofocus>
									<div class="invalid-feedback">
										Warna Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tahun</label>
								<div class="col-sm-12 col-md-7">
									<input id="name" type="text" class="form-control" name="tahun" tabindex="1" placeholder="2022" required autofocus>
									<div class="invalid-feedback">
										Tahun Kendaraan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Polisi</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control nopol" name="nopol" placeholder="AD 3452 IH" required autofocus>
									<div class="invalid-feedback">
										Nomor Polisi Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelengkapan</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="kelengkapan" class="summernote"></textarea>
									<div class="invalid-feedback">
										Kelengkapan Masih Kosong
									</div>
									<small>Diskripsikan Kelengkapan Kendaraan</small>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kerusakan</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="kerusakan" class="summernote"></textarea>
									<div class="invalid-feedback">
										Kerusakan Masih Kosong
									</div>
									<small>Diskripsikan Kerusakan Kendaraan</small>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Solusi</label>
								<div class="col-sm-12 col-md-7">
									<textarea name="solusi" class="summernote"></textarea>
									<div class="invalid-feedback">
										Solusi Masih Kosong
									</div>
									<small>Diskripsikan Solusi Kerusakan Kendaraan</small>
								</div>
							</div>					
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambahkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>



<script>
	var cleave = new Cleave('.nopol', {
		numericOnly: false,
		delimiters: [' ', ' ', ' '],
		blocks: [2, 4, 4]
	});

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>