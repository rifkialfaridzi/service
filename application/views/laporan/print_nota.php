<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Nota</title>

    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
    <style>
        html {
            margin: 10px 0 10px 0
        }

        h3 {
            font-size: 18px;
        }

        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg .tg-lboi {
            border-color: inherit;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div class="container">
        <style type="text/css">
            .tg {
                border-collapse: collapse;
                border-spacing: 0;
            }

            .tg td {
                font-family: Arial, sans-serif;
                font-size: 14px;
                padding: 10px 5px;

                word-break: normal;

            }

            .tg th {
                font-family: Arial, sans-serif;
                font-size: 14px;
                font-weight: normal;
                padding: 10px 5px;
                overflow: hidden;
                word-break: normal;
            }

            .tg .tg-0pky {
                border-color: inherit;
                text-align: left;
                vertical-align: top
            }
        </style>
        <table class="tg" style="table-layout: fixed; width: 100%; border:solid 1px">
            <colgroup>
                <col style="width: 20%">
                <col style="width: 60%">
                <col style="width: 20%">
            </colgroup>
            <tr>
                <th class="tg-0pky">
                    <h1>BENGKEL SLAMET</h1>
                    <p style="margin:5px 0 5px 0;font-size:12px; vertical-align: middle;">Jl. Parang Barong No.1, Dusun IV, Makamhaji, Kec. Kartasura, Kabupaten Sukoharjo 57161</p>
                </th>
                <th class="tg-0pky" style="text-align:center;vertical-align: middle">
                    <h4> Nota Penjualan </h4>
                </th>
                <th class="tg-0pky" style="text-align: center">
                    <p style="margin: 2px 0 2px 0;font-size: 8"><?php echo $data_service[0]->kode_transaksi; ?></p>
                    <p style="margin: 2px 0 2px 0;font-size: 8"><?php echo "Ahmad Nur Cahyo - Admin"; //echo $this->session->userdata['username']; ?></p>
                </th>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="3">
                    <table class="tg" style="width:100%">
                        <tr>
                            <th class="tg-cly1" colspan="5">
                                <table style="padding:0">
                                    <tr>
                                        <th style="padding:2px">
                                            <p style="margin:0;">Kepada</p>
                                        </th>
                                        <th style="padding:2px">
                                            <p style="margin:0;">: <?php echo $data_service[0]->nama_customer; ?></p>
                                            </p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:2px">
                                            <p style="margin:0;">STATUS TRANSAKSI</p>
                                        </th>
                                        <th style="padding:2px">
                                            <?php

                                            switch ($data_service[0]->status) {
                                                case 0:
                                                    $pesan = "Penrimaan kendaraan";
                                                    break;
                                                case 1:
                                                    $pesan = "Dalam Peengecekan Teknisi";
                                                    break;
                                                case 2:
                                                    $pesan = "Penggantian Sparepart Disetujui";
                                                    break;
                                                case 3:
                                                    $pesan = "Proses Pemasangan Sparepart";
                                                    break;
                                                case 4:
                                                    $pesan = "Kendaraan Bisa Di Ambil";
                                                    break;
                                                case 5:
                                                    $pesan = "Transaksi Selesai";
                                                    break;
                                                case 6:
                                                    $pesan = "Transaksi Di Batalkan";
                                                    break;
                                                default:
                                                    # code...
                                                    break;
                                            }



                                            ?>
                                            <p style="margin:0;">: <?php echo $pesan; ?></p>
                                        </th>
                                    </tr>
                        </tr>
                    </table>
                    </th>
            </tr>
            <tr>
                <td style="border-top:1 solid black;padding:2px;width:5%">
                    <p style="font-weight:bold;margin:0">No.</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:50%">
                    <p style="font-weight:bold;margin:0">Keterangan</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:45%">
                    <p style="font-weight:bold;margin:0">Harga</p>
                </td>
            </tr>

            <?php $nomor = 1;
            $totalBayar = 0;
            foreach ($detail_service as $key) {
                $totalBayar +=  intval($key->harga); ?>
                <tr>
                    <td class="tg-cly1" style="border-top:1 solid black;border-bottom:1 solid black;padding:2px;text-align:center"><?php echo $nomor; ?></td>
                    <td class="tg-cly1" style="border:solid 1px;padding:2px"><?php echo $key->item; ?></td>
                    <td class="tg-0lax" style="border:solid 1px;padding:2px">Rp. <?php echo number_format($key->harga, 2, ',', '.'); ?></td>
                </tr>
            <?php } ?>

            <tr>

                <td class="tg-0lax" colspan="2" style="border:none">
                    <p style="text-align:right;font-size: 15px">Total Pembayaran </p>
                </td>
                <td>
                    <p style="font-weight:bold;font-size: 15px">Rp. <?php echo number_format($totalBayar, 2, ',', '.'); ?></p>
                </td>
            </tr>

            <tr>

                <td class="tg-0lax" colspan="2">
                    <p style="text-align:left;margin-top:0;">
                        Tanggal Penerimaan Kendaraan :
                        <b><?php $time = strtotime($data_service[0]->tanggal_terima);
                            echo date('d M Y', $time); ?></b>
                        <br>Tanggal Pengambilan Kendaraan :
                        <b><?php
                            if ($data_service[0]->tanggal_ambil == "0000-00-00") {
                                echo "Belum Diambil";
                            } else {
                                $time = strtotime($data_service[0]->tanggal_ambil);
                                echo date('d M Y', $time);
                            }
                            ?>
                        </b>
                    </p>

                    <p style="text-align:left;margin-top:0;">
                        <b>Keterangan</b> Komplain Service hanya berlaku garansi 5 hari stelah pengambilan barang
                    </p>



                </td>
                <td class="tg-0lax" style="text-align: right;">
                    <p>
                        <b>Akses Login Untuk Cek Service</b><br>
                        <?php echo base_url(); ?><br>
                        <b>Username :</b> <?php echo str_replace(" ", "", $data_service[0]->nomor_telepon); ?><br>
                        <b>Password :</b> password
                    </p>
                </td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax">
                    <p style="font-weight:bold"></p>
                </td>
            </tr>
            <tr>

                <td class="tg-0lax" colspan="2">
            

                </td>
                <td class="tg-0lax" style="text-align: center;">
                <br>
                <b>Penerima</b>
                </td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax">
                    <p style="font-weight:bold"></p>
                </td>
            </tr>
            <tr>

                <td class="tg-0lax" colspan="2">
                </td>
                <td class="tg-0lax" style="text-align: center;">
                <br>
                <br>
                <hr style="border:1px black dashed;width:50%">
                </td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax">
                    <p style="font-weight:bold"></p>
                </td>
            </tr>
        </table>
        </td>

        </td>
        </tr>
        </table>

    </div>
</body>

</html>