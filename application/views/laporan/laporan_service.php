<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Service</title>
</head>
<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:center">
            <!-- <img style="width: auto;height:150px" src="<?php // echo base_url('assets/img/logo.png') ?>"></img> -->
            <h1>BENGKEL SLAMET</h1>
            <small>Jl. Parang Barong No.1, Dusun IV, Makamhaji, Kec. Kartasura, Kabupaten Sukoharjo 57161</small>
            <h4>Laporan Service</h4>
        </td>
    </tr>
</tbody>

</table>

<hr>

<div style="text-align:center">

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>

            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode Service</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Customer</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kendaraan</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nomor Polisi</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal Masuk</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Biaya</b></td>
        </tr>

        <?php $totalBiaya = 0; foreach ($data_service as $key) { 
            $totalBiaya += $key->total_biaya;
        ?>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->kode_transaksi; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nama_customer; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nama_kendaraan; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nopol; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->tanggal_terima; ?></td>
        <td style="text-align:right; padding: 2px 5px 2px 5px"><?php echo "Rp. ". number_format($key->total_biaya, 0, ',', '.'); ?></td>
        </tr>
        <?php } 
        ?>
        <tr>
            <td colspan="5" style="text-align:center; font-weight: bold;"> Total </td>
            <td style="text-align:right; font-weight: bold;padding: 2px 5px 2px 5px"> <?php echo "Rp. ". number_format($totalBiaya, 0, ',', '.'); ?> </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>
            <td>Yang Mengetahui,</td>
        </tr>
        <tr>
            <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Pimpinan<br>Slamet Raharjo</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>