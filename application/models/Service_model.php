<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_model extends CI_Model
{

    public $table = 'transaksi';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->db->select('t.*,u.nama as nama_customer, u.nomor_telepon as nomor_telepon, u.alamat as alamat');
        $this->db->from('transaksi t');
        $this->db->join('user_detail u', 't.user=u.user', 'left');
        return $this->db->get()->result();
    }

    function data_service_by_pengguna($id) {
        
        $this->db->select('t.*,u.nama as nama_customer, u.nomor_telepon as nomor_telepon, u.alamat as alamat');
        $this->db->from('transaksi t');
        $this->db->join('user_detail u', 't.user=u.user', 'left');
        $this->db->where('t.user', $id);
        return $this->db->get()->result();
    }

    function data_service_by_order($periode, $status) {

        $time = strtotime($periode);
        $month = date('m', $time);
        $year = date('Y', $time);
        

        $this->db->select('t.*,u.nama as nama_customer, u.nomor_telepon as nomor_telepon, u.alamat as alamat');
        $this->db->from('transaksi t');
        $this->db->join('user_detail u', 't.user=u.user', 'left');
        $this->db->where('MONTH(t.tanggal_terima)', $month);
        $this->db->where('YEAR(t.tanggal_terima)', $year);
        $this->db->where('t.status', $status);
        return $this->db->get()->result();
    }

    function data_service_by_status($periode) {

        $time = strtotime($periode);
        $month = date('m', $time);
        $year = date('Y', $time);
        

        $this->db->select('t.*,u.nama as nama_customer, u.nomor_telepon as nomor_telepon, u.alamat as alamat');
        $this->db->from('transaksi t');
        $this->db->join('user_detail u', 't.user=u.user', 'left');
        $this->db->where('MONTH(t.tanggal_terima)', $month);
        $this->db->where('YEAR(t.tanggal_terima)', $year);
        return $this->db->get()->result();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


    // get data by id
    function get_by_id($id)
    {
        $this->db->select('t.*,u.nama as nama_customer, u.nomor_telepon as nomor_telepon, u.alamat as alamat');
        $this->db->from('transaksi t');
        $this->db->join('user_detail u', 't.user=u.user', 'left');
        $this->db->where('t.id', $id);
        return $this->db->get()->result();
    }

    function log_get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('log_transaksi');
        $this->db->where('transaksi', $id);
        return $this->db->get()->result();
    }

    function get_detail_transaksi_by_id($id)
    {
        $this->db->select('dt.*');
        $this->db->from('transaksi t');
        $this->db->join('detail_transaksi dt', 't.id=dt.transaksi', 'left');
        $this->db->where('dt.transaksi', $id);
        return $this->db->get()->result();
    }

    function detail_service($id)
    {
        $this->db->select('dt.*');
        $this->db->from('transaksi t');
        $this->db->join('detail_transaksi dt', 't.id=dt.transaksi', 'left');
        $this->db->where('t.id', $id);
        return $this->db->get()->result();
    }

    function total_bayar_service($id)
    {
        $this->db->select('SUM(dt.harga) as total_biaya');
        $this->db->from('transaksi t');
        $this->db->join('detail_transaksi dt', 't.id=dt.transaksi', 'left');
        $this->db->where('t.id', $id);
        return $this->db->get()->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function insert_item($data)
    {
        $this->db->insert("detail_transaksi", $data);
    }

    function insert_log($data)
    {
        $this->db->insert("log_transaksi", $data);
    }

    function insert($data)
    {

        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    // update data
    function update($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update($this->table, $data);
    }
    // delete data
    function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->delete($this->table);
    }

    function delete_item($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("detail_transaksi");
    }
}

/* End of file Category_model.php */
/* Location: ./application/models/Category_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */