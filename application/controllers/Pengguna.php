<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		// if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
		// 	redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		// }

		// ADDONS

		$this->load->model('Service_model');
		$this->load->model('User_detail_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['dataPengguna'] = $this->User_detail_model->get_by_idPengguna($data_session['id'])->result();

		if (empty($data['dataPengguna'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'pengguna/detail';
			$data['page_title'] = 'Halaman Detail Pengguna';
			$this->load->view('template', $data);
		}
	}

	public function data_service_by_pengguna($id)
	{
		header('Content-Type: application/json');
		$data_service =  $this->Service_model->data_service_by_pengguna($id);

		$Service = [];
		foreach ($data_service as $key) {
			$nominal = $this->Service_model->total_bayar_service($key->id)[0]->total_biaya;
			$key->total_biaya =  $nominal == null ? 0 : $nominal;
			$Service[] = $key;
		}

		

		$data['draw'] = 0;
		$data['recordsTotal'] = $Service == null ? [] : count($Service);
		$data['recordsFiltered'] = $Service == null ? [] : count($Service);
		$data['data'] = $Service == null ? [] : $Service;

		echo json_encode($data);
	}

	public function index()
	{
		// || $data_session['level'] != 1 && $data_session['level'] != 2
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'pengguna/main';
		$data['page_title'] = 'Halaman Pengguna';
		$this->load->view('template', $data);
	}

	public function data_absen($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_absen_masuk = $this->User_detail_model->record_absen_by_user($id, $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['recordsFiltered'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['data'] = $data_absen_masuk == null ? [] : $data_absen_masuk;
				}
				echo json_encode($data);
	}

	public function data_ijin($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
		if (is_null($month)) {
			$data['draw'] = 0;
			$data['recordsTotal'] = 0;
			$data['recordsFiltered'] = 0;
			$data['data'] = [];
		}else{
			$data_ijin = $this->User_detail_model->record_ijin_by_user($id, $month);
			$data['draw'] = 0;
			$data['recordsTotal'] = $data_ijin == null ? [] : count($data_ijin);
			$data['recordsFiltered'] = $data_ijin == null ? [] : count($data_ijin);
			$data['data'] = $data_ijin == null ? [] : $data_ijin;
		}
		echo json_encode($data);
	}

	public function print_absen($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
				if (is_null($month) || is_null($month)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Ditemkan');
					redirect(site_url('pengguna'));
				}else{
					$data_Pengguna = $this->User_detail_model->get_by_id($id);
					$data_absen_masuk = $this->User_detail_model->record_absen_by_user($id, $month);

					if (is_null($data_absen_masuk)) {
						$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
						redirect(site_url('pengguna'));
					}else{

						$data['Pengguna'] = $data_Pengguna;
						$data['data_absen_masuk'] = $data_absen_masuk;
						$data['periode'] = $month;
				
						$this->load->library('pdf');

						$this->pdf->setPaper('A4', 'potrait');
						$this->pdf->set_option('isRemoteEnabled', TRUE);
						$this->pdf->filename = "Laporan Absensi Pengguna-" . $data_Pengguna[0]->nama . "-" . $data_Pengguna[0]->jabatan . ".pdf";
						$this->pdf->load_view('laporan/laporan_absensi', $data);

					}
				}
	}

	public function detail($id)
	{
		$data['id_user'] = $id;
		$data['main_content'] = 'pengguna/detail';
		$data['page_title'] = 'Halaman Detail Pengguna';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data['main_content'] = 'pengguna/create';
		$data['page_title'] = 'Halaman Pengguna';
		$this->load->view('template', $data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$Pengguna =  $this->User_detail_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Pengguna == null ? [] : count($Pengguna);
		$data['recordsFiltered'] = $Pengguna == null ? [] : count($Pengguna);
		$data['data'] = $Pengguna == null ? [] : $Pengguna;

		echo json_encode($data);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('pengguna'));
			//echo validation_errors();
		} else {

			$data_post = $this->input->post();

			if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

				// DO UPLOAD
				$file_name = str_replace('.', '', random_string('alnum', 10));
				$config['upload_path'] = './assets/uploads/';
				$config['allowed_types']        = 'gif|jpg|jpeg|png';
				$config['file_name']            = $file_name;
				$config['overwrite']            = true;
				$config['max_size']             = 1024; // 1MB
				$config['max_width']            = 1080;
				$config['max_height']           = 1080;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto')) {
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
					redirect(site_url('pengguna'));
				} else {
					$uploaded_data = $this->upload->data();
					$data_post['foto'] = $uploaded_data['file_name'];
				}
			} else {
				$data_post['foto'] = "dummy.jpg";
			}

			$data_Pengguna['username'] = str_replace(" ","",$this->input->post("nomor_telepon"));
			$data_Pengguna['password'] = md5("password");
			$data_Pengguna['level'] = 3;
			$data_Pengguna['created_at'] = date("Y-m-d");

			// Insert Data Pengguna
			$lastest_id = $this->User_detail_model->insert_user($data_Pengguna);

			$data_post['user'] = $lastest_id;
			$data_post['tanggal_masuk'] = date("Y-m-d");

			$this->User_detail_model->insert_Pengguna($data_post);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pengguna'));
		}
	}

	public function edit($id)
	{

		$row = $this->User_detail_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_pengguna' => $row,
				'main_content' => 'pengguna/update',
				'page_title' => 'Edit Pengguna'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('pengguna'));
		}
	}

	public function update_action($id, $type = null)
	{
		if (is_null($type)) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>');
			redirect(site_url('pengguna'));
		} else {
			if ($type == "profile") {

				$Pengguna = $this->User_detail_model->get_by_id($id);

				if (empty($Pengguna)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan </br>');
					redirect(site_url('pengguna'));
				} else {

					// FORM VALIDATION
					$this->form_validation->set_rules('nama', 'Nama', 'required');
					$this->form_validation->set_rules('alamat', 'Alamat', 'required');

					$is_nomor_telepon = $this->input->post('nomor_telepon', TRUE) != $Pengguna[0]->nomor_telepon ? '|is_unique[user_detail.nomor_telepon	]' : '';
					$this->form_validation->set_rules('nomor_telepon', 'Nomor Telepon', 'required' . $is_nomor_telepon);

					if ($this->form_validation->run() == FALSE) {
						$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
						redirect(site_url('pengguna/edit/' . $id));

					} else {

					$data_post = $this->input->post();

					if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

						// DO UPLOAD
						$file_name = str_replace('.', '', random_string('alnum', 10));
						$config['upload_path'] = './assets/uploads/';
						$config['allowed_types']        = 'gif|jpg|jpeg|png';
						$config['file_name']            = $file_name;
						$config['overwrite']            = true;
						$config['max_size']             = 1024; // 1MB
						$config['max_width']            = 1080;
						$config['max_height']           = 1080;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('foto')) {
							$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
							redirect(site_url('pengguna'));
						} else {
							$uploaded_data = $this->upload->data();
							$data_post['foto'] = $uploaded_data['file_name'];
						}

					} else {
						$data_post['foto'] = $Pengguna[0]->foto;
					}


					$this->User_detail_model->update($id, $data_post);
					$this->User_detail_model->update_profile($id, ["username" => str_replace(" ","",$this->input->post("nomor_telepon"))]);
					$this->session->set_flashdata('pesan', 'Data Sukses Diubah</br>');
					redirect(site_url('pengguna/edit/' . $id));
					}
				}
			} elseif ($type == "akun") {
				$Pengguna = $this->User_detail_model->get_by_id($id);
				$is_username = $this->input->post('username', TRUE) != $Pengguna[0]->username ? '|is_unique[user.username]' : '';
				$this->form_validation->set_rules('username', 'Username', 'required' . $is_username);

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
					redirect(site_url('pengguna/edit/' . $id));
				} else {

					if (empty($this->input->post('password'))) {
						$this->User_detail_model->update_profile($id, ['username' => $this->input->post('username')]);
					} else {
						$this->User_detail_model->update_profile($id, ['username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))]);
					}

					$this->session->set_flashdata('pesan', 'Data Sukses Diubah');
					redirect(site_url('pengguna/edit/' . $id));
				}
			} else {
				$this->session->set_flashdata('pesan', 'Data Gagal Diubah : Method Tidak Diketahui </br>');
				redirect(site_url('pengguna'));
			}
		}
	}

	public function delete($id)
	{
		$row = $this->User_detail_model->get_by_id($id);

		if ($row) {
			$this->User_detail_model->delete($id);
			$this->User_detail_model->delete_user($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pengguna'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pengguna'));
		}
	}

	public function print_Pengguna($id = null) // CETAK LAPORAN 
	{
		$data_session = $this->session->userdata;

		if ($id == null) {
			$dataPengguna = $this->User_detail_model->get_by_idPengguna($data_session['id'])->result();
		} else {
			$dataPengguna = $this->User_detail_model->get_by_idPengguna($id)->result();
		}


		if ($dataPengguna == null) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('lineup'));
		} else {
			//var_dump($dataPengguna);
			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Laporan Pengguna-" . $dataPengguna[0]->name . "-" . $dataPengguna[0]->posisi . ".pdf";
			$this->pdf->load_view('laporan/laporan_Pengguna', ['data' => $dataPengguna]);
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nomor_telepon', 'Nomor Telepon', 'required|is_unique[user_detail.nomor_telepon]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
