<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Gaji extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Gaji_model');
		$this->load->model('Ijin_model');
		$this->load->model('Pegawai_model');
		$this->load->model('Profile_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main_data()
	{
		header('Content-Type: application/json');
		$Gaji =  $this->Gaji_model->main_data();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Gaji == null ? [] : count($Gaji);
		$data['recordsFiltered'] = $Gaji == null ? [] : count($Gaji);
		$data['data'] = $Gaji == null ? [] : $Gaji;

		echo json_encode($data);
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'gaji/main';
		$data['page_title'] = 'Halaman Detail Jabatan';
		$this->load->view('template', $data);
	}

	public function update_status($type, $id)
	{
		$this->Gaji_model->update_status($id, ["status" => $type]);
		$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
		redirect(site_url('Gaji/main'));
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] == 1 && $data_session['level'] == 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'Gaji/main';
		$data['page_title'] = 'Halaman Detail Penggajian';
		$this->load->view('template', $data);
	}

	function _toInt($str)
	{
		return (int)preg_replace("/([^0-9\\.])/i", "", $str);
	}
	public function detail_gaji($id_gaji, $user)
	{

		$gaji = $this->Gaji_model->get_by_id($id_gaji);

		if ($gaji) {

			$periode = $gaji->periode;
			$time = strtotime($periode);
			$month = date('m', $time);


			$jumlah_hari = $gaji->jumlah_hari;
			$lembur = $gaji->lembur;

			// GET ALL PEGAWAI
			$semua_pegawai = $this->Pegawai_model->get_by_id($user);

			$data = [];
				// BASIC DATA
				$data_gaji["id_gaji"] = $id_gaji;
				$data_gaji["id_user"] = $semua_pegawai[0]->user;
				$data_gaji["nama"] = $semua_pegawai[0]->nama;
				$data_gaji["jabatan"] = $semua_pegawai[0]->jabatan;
				$data_gaji["nik"] = $semua_pegawai[0]->nik;
				$data_gaji["gaji_pokok"] = $semua_pegawai[0]->gaji_pokok;
				// END BASIC DATA

				//DATA ABSENSI

				$data_gaji["hari_kerja"] = $jumlah_hari;

				$jumlah_hari_kerja = $this->Profile_model->valid_record_absen($semua_pegawai[0]->user, "absen pulang", $month);
				$data_gaji["jumlah_hari_kerja"] = is_null($jumlah_hari_kerja) ? 0 : $jumlah_hari_kerja->jumlah_absen;

				$cuti = $this->Ijin_model->valid_record_ijin($semua_pegawai[0]->user, $month, "cuti");
				$ijin = $this->Ijin_model->valid_record_ijin($semua_pegawai[0]->user, $month, "ijin");
				$data_gaji["jumlah_cuti"] = (int)is_null($cuti) ? 0 : $cuti->total_hari;
				$data_gaji["jumlah_ijin"] = (int)is_null($ijin) ? 0 : $ijin->total_hari;

				$data_gaji["jumlah_hari_kerja_ijin_cuti"] = (int)$data_gaji["jumlah_hari_kerja"] + $data_gaji["jumlah_cuti"] + $data_gaji["jumlah_ijin"];

				$data_gaji["jumlah_hari_tidakmasuk"] = (int)$jumlah_hari - $data_gaji["jumlah_hari_kerja_ijin_cuti"];

				$data_gaji["gaji"] = round(intval($data_gaji["jumlah_hari_kerja_ijin_cuti"]) / intval($jumlah_hari), 2) * $this->_toInt($semua_pegawai[0]->gaji_pokok);
				// $data_gaji["gaji"] = (int)((intval($data_gaji["jumlah_hari_kerja_ijin_cuti"]) / intval($jumlah_hari)) * intval($data_gaji["gaji_pokok"]));
				//END DATA ABSENSI
				// DATA LEMBUR 
				$jumlah_jam_lembur =  $this->Profile_model->valid_record_lembur($semua_pegawai[0]->user, $month);
				$data_gaji["jumlah_jam_lembur"] = is_null($jumlah_jam_lembur->jumlah_jam) ? 0 : $jumlah_jam_lembur->jumlah_jam;
				$data_gaji["total_bayar_lembur"] = $data_gaji["jumlah_jam_lembur"] * $lembur;
				$data_gaji["total_gaji"] = $data_gaji["gaji"] + $data_gaji["total_bayar_lembur"];

				// END DATA LEMBUR

				return $data[] = $data_gaji;
		}
	}

	public function detail($id)
	{

		$gaji = $this->Gaji_model->get_by_id($id);

		if ($gaji) {
			$periode = $gaji->periode;
			$time = strtotime($periode);
			$month = date('m', $time);


			$jumlah_hari = $gaji->jumlah_hari;
			$lembur = $gaji->lembur;

			// GET ALL PEGAWAI
			$semua_pegawai = $this->Pegawai_model->get_pegawai();
			//$absen = $this->Profile_model->valid_record_absen(45, "absen pulang", $month);
			//$ijin = $this->Ijin_model->valid_record_ijin(45, $month, "cuti");
			//var_dump($this->Ijin_model->valid_record_ijin(45, $month, "cuti")->total_hari);

			// $absen = $this->Profile_model->valid_record_lembur(45, $month);

			//  var_dump($absen);

			$data = [];
			// var_dump($this->_toInt($semua_pegawai[0]->gaji_pokok));
			foreach ($semua_pegawai as $semua_pegawai) {

				//Penghitungan Jumlah Absen Pulang

				// BASIC DATA
				$data_gaji["id_gaji"] = $id;
				$data_gaji["id_user"] = $semua_pegawai->user;
				$data_gaji["nama"] = $semua_pegawai->nama;
				$data_gaji["jabatan"] = $semua_pegawai->jabatan;
				$data_gaji["nik"] = $semua_pegawai->nik;
				$data_gaji["gaji_pokok"] = $semua_pegawai->gaji_pokok;
				// END BASIC DATA

				//DATA ABSENSI

				$data_gaji["hari_kerja"] = $jumlah_hari;

				$jumlah_hari_kerja = $this->Profile_model->valid_record_absen($semua_pegawai->user, "absen pulang", $month);
				$data_gaji["jumlah_hari_kerja"] = is_null($jumlah_hari_kerja) ? 0 : $jumlah_hari_kerja->jumlah_absen;

				$cuti = $this->Ijin_model->valid_record_ijin($semua_pegawai->user, $month, "cuti");
				$ijin = $this->Ijin_model->valid_record_ijin($semua_pegawai->user, $month, "ijin");
				$data_gaji["jumlah_cuti"] = (int)is_null($cuti) ? 0 : $cuti->total_hari;
				$data_gaji["jumlah_ijin"] = (int)is_null($ijin) ? 0 : $ijin->total_hari;

				$data_gaji["jumlah_hari_kerja_ijin_cuti"] = (int)$data_gaji["jumlah_hari_kerja"] + $data_gaji["jumlah_cuti"] + $data_gaji["jumlah_ijin"];

				$data_gaji["jumlah_hari_tidakmasuk"] = (int)$jumlah_hari - $data_gaji["jumlah_hari_kerja_ijin_cuti"];

				$data_gaji["gaji"] = round(intval($data_gaji["jumlah_hari_kerja_ijin_cuti"]) / intval($jumlah_hari), 2) * $this->_toInt($semua_pegawai->gaji_pokok);
				// $data_gaji["gaji"] = (int)((intval($data_gaji["jumlah_hari_kerja_ijin_cuti"]) / intval($jumlah_hari)) * intval($data_gaji["gaji_pokok"]));
				//END DATA ABSENSI
				// DATA LEMBUR 
				$jumlah_jam_lembur =  $this->Profile_model->valid_record_lembur($semua_pegawai->user, $month);
				$data_gaji["jumlah_jam_lembur"] = is_null($jumlah_jam_lembur->jumlah_jam) ? 0 : $jumlah_jam_lembur->jumlah_jam;
				$data_gaji["total_bayar_lembur"] = $data_gaji["jumlah_jam_lembur"] * $lembur;
				$data_gaji["total_gaji"] = $data_gaji["gaji"] + $data_gaji["total_bayar_lembur"];

				// END DATA LEMBUR

				$data[] = $data_gaji;

			}

			$data = array(
				'data_gaji' => $data,
				'main_content' => 'gaji/detail',
				'page_title' => 'Detai Periode Gaji'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('Gaji'));
		}
	}



	public function pegawai($id_gaji, $id_user)
	{

		$gaji = $this->Gaji_model->get_by_id($id_gaji);
		$pegawai = $this->Pegawai_model->get_by_id($id_user);

		if ($gaji && $pegawai) {
			
			$data_gaji = $this->detail_gaji($id_gaji, $id_user);


			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "slip_gaji-" . $pegawai[0]->nama . "-" . $gaji->periode . ".pdf";
			$this->pdf->load_view('laporan/slip_gaji', ['data_gaji' => $data_gaji, 'pegawai' => $pegawai, 'periode' => $gaji]);
		
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('Gaji'));
		}
	}

	public function periode()
	{

		$data_session = $this->session->userdata;
		// if ((!$this->session->userdata('logged_in')) || $data_session['level'] == 1 && $data_session['level'] == 2) {
		// 	redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		// }

		$data['id_user'] = $data_session['id'];
		$data['main_content'] = 'Gaji/periode';
		$data['page_title'] = 'Halaman Detail Periode Gaji';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'gaji/create';
		$data['page_title'] = 'Halaman Penggajian';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');
		$data_session = $this->session->userdata;
		$Gaji =  $this->Gaji_model->json($data_session['id']);

		$data['draw'] = 0;
		$data['recordsTotal'] = $Gaji == null ? [] : count($Gaji);
		$data['recordsFiltered'] = $Gaji == null ? [] : count($Gaji);
		$data['data'] = $Gaji == null ? [] : $Gaji;

		echo json_encode($data);
	}


	public function create_action()
	{


		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('gaji'));
			//echo validation_errors();
		} else {

			if ($this->Gaji_model->check_month($this->input->post("periode"))) {
				$this->session->set_flashdata('pesan', 'Data Sudah Ada');
				redirect(site_url('gaji'));
			} else {
				$this->Gaji_model->insert($this->input->post());
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect(site_url('gaji'));
			}
		}
	}

	public function edit($id)
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$row = $this->Gaji_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_gaji' => $row,
				'main_content' => 'Gaji/update',
				'page_title' => 'Edit Gaji'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('Gaji'));
		}
	}

	public function update_action($id)
	{

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('Gaji'));
		} else {
			$this->Gaji_model->update($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('Gaji'));
		}
	}

	public function delete($id)
	{
		$row = $this->Gaji_model->get_by_id($id);

		if ($row) {
			$this->Gaji_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('Gaji'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('Gaji'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('periode', 'Periode', 'required');
		$this->form_validation->set_rules('jumlah_hari', 'Jumlah Hari', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
