<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;


		$this->load->model('Service_model');
		$this->load->model('User_detail_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function generate_code()
	{
		$query = "SELECT max(kode_transaksi) as maxKode FROM transaksi";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "TRX";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
	}

	public function main()
	{

		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'profile/service';
		$data['page_title'] = 'Halaman Service';
		$this->load->view('template', $data);
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		//|| $data_session['level'] != 1 && $data_session['level'] != 2
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'service/main';
		$data['page_title'] = 'Halaman Service';
		$this->load->view('template', $data);
	}

	public function create()
	{
		//var_dump($this->generate_code());
		$data['customer'] = $this->User_detail_model->get_by_customer();
		$data['main_content'] = 'service/create';
		$data['page_title'] = 'Halaman Service';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');
		$Service =  $this->Service_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Service == null ? [] : count($Service);
		$data['recordsFiltered'] = $Service == null ? [] : count($Service);
		$data['data'] = $Service == null ? [] : $Service;

		echo json_encode($data);
	}

	public function short_data_service()
	{
		header('Content-Type: application/json');

		$order_status = $this->input->get("status");

		if ($order_status == "all") {
			$periode = date("Y") . "-" . "0" . $this->input->get("periode") . "-" . "01";
			$Service =  $this->Service_model->data_service_by_status($periode);
		} else {
			$periode = date("Y") . "-" . "0" . $this->input->get("periode") . "-" . "01";
			$Service =  $this->Service_model->data_service_by_order($periode, $order_status);
		}

		$data['draw'] = 0;
		$data['recordsTotal'] = $Service == null ? [] : count($Service);
		$data['recordsFiltered'] = $Service == null ? [] : count($Service);
		$data['data'] = $Service == null ? [] : $Service;

		echo json_encode($data);
	}

	public function data_service_by_pengguna()
	{
		header('Content-Type: application/json');
		$data_session = $this->session->userdata;
		$Service =  $this->Service_model->data_service_by_pengguna($data_session['id']);

		$data['draw'] = 0;
		$data['recordsTotal'] = $Service == null ? [] : count($Service);
		$data['recordsFiltered'] = $Service == null ? [] : count($Service);
		$data['data'] = $Service == null ? [] : $Service;

		echo json_encode($data);
	}

	public function add_item_action($id)
	{
		//var_dump($this->input->post());

		$this->form_validation->set_rules('item', 'Nama Item', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('service/edit/') . $id);
			//echo validation_errors();
		} else {
			$data_service = $this->input->post();
			$data_service['transaksi'] = $id;
			$this->Service_model->insert_item($data_service);
			$this->session->set_flashdata('pesan', 'Data Sukses Ditambahkan');
			redirect(site_url('service/edit/') . $id);
		}
	}

	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('service'));
			//echo validation_errors();
		} else {
			$data_service = $this->input->post();
			$data_service['kode_transaksi'] = $this->generate_code();
			$data_service['tanggal_terima'] = date('Y-m-d');
			$data_service['penerima'] = 45;
			$data_service['tanggal_ambil'] = date('0000-00-00');
			$data_service['status'] = 0;
			$data_service['biaya'] = 0;
			$data_service['kondisi_barang'] = "-";


			$last_id = $this->Service_model->insert($data_service);

			$data_log["transaksi"] = $last_id;
			$data_log["status"] = "Penerimaan Kendaraan";
			$data_log["tanggal"] = date("Y-m-d H:i:s");
			$this->Service_model->insert_log($data_log);

			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('service'));
		}
	}

	public function edit($id)
	{

		$row = $this->Service_model->get_by_id($id);
		if ($row) {


			$data = array(
				'customer' => $this->User_detail_model->get_by_customer(),
				'data_service' => $row,
				'detail_service' => $this->Service_model->detail_service($id),
				'total_bayar' => $this->Service_model->total_bayar_service($id)[0]->total_biaya == null ? 0 : $this->Service_model->total_bayar_service($id)[0]->total_biaya,
				'main_content' => 'service/update',
				'page_title' => 'Edit Service'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('service'));
		}
	}

	public function lookup($id)
	{

		$row = $this->Service_model->get_by_id($id);
		if ($row) {
			$log_transaksi = $this->Service_model->log_get_by_id($id);

			$data = array(
				'customer' => $this->User_detail_model->get_by_customer(),
				'log_transaksi' => $log_transaksi,
				'data_service' => $row,
				'detail_service' => $this->Service_model->detail_service($id),
				'total_bayar' => $this->Service_model->total_bayar_service($id)[0]->total_biaya == null ? 0 : $this->Service_model->total_bayar_service($id)[0]->total_biaya,
				'main_content' => 'service/lookup',
				'page_title' => 'Detail Service'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('service'));
		}
	}

	public function detail($id)
	{

		$row = $this->Service_model->get_by_id($id);
		if ($row) {

			$data = array(
				'customer' => $this->User_detail_model->get_by_customer(),
				'data_service' => $row,
				'main_content' => 'service/detail',
				'page_title' => 'Edit Service'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('service'));
		}
	}

	public function print_nota($id)
	{
		$row = $this->Service_model->get_by_id($id);
		if ($row) {
			$detail_service = $this->Service_model->detail_service($id);
			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Nota_Service-" . $row[0]->nama_customer . "-" . $row[0]->tanggal_terima . ".pdf";
			$this->pdf->load_view('laporan/print_nota', ['data_service' => $row, "detail_service" => $detail_service]);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('service'));
		}
	}

	public function laporan_service()
	{
		$row = $this->Service_model->json();
		if ($row) {

			$order_status = $this->input->get("status");

			if ($order_status == "all") {
				$periode = date("Y") . "-" . "0" . $this->input->get("periode") . "-" . "01";
				$data_service =  $this->Service_model->data_service_by_status($periode);
			} else {
				$periode = date("Y") . "-" . "0" . $this->input->get("periode") . "-" . "01";
				$data_service =  $this->Service_model->data_service_by_order($periode, $order_status);
			}

			$Service = [];
			foreach ($data_service as $key) {
				$nominal = $this->Service_model->total_bayar_service($key->id)[0]->total_biaya;
				$key->total_biaya =  $nominal == null ? 0 : $nominal;
				$Service[] = $key;
			}


			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Laporan_Service.pdf";
			$this->pdf->load_view('laporan/laporan_service', ['data_service' => $Service]);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('service'));
		}
	}

	public function send_wa($id)
	{

		$row = $this->Service_model->get_by_id($id);
		if ($row) {

			$nomorWA = str_replace(" ", "", $row[0]->nomor_telepon);
			$pesan = "";
			switch ($row[0]->status) {
				case 0:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Sudah Kami Terima* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 1:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Dalam Pengecekan* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 2:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Penggantian Sparepart Disetujui* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 3:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Proses Pemasangan Sparepart* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 4:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Bisa Di Ambil* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 5:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Transaksi Selesai* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				case 6:
					$pesan = "Halo kak, *" . $row[0]->nama_customer . "* Transaksi Anda Yang bernomor *" . $row[0]->kode_transaksi . "* . \n Memberitahukan Bahwa Kendaraan anda *" . $row[0]->nama_kendaraan . "* Warna *" . $row[0]->warna . "* Bernomor Polisi *" . $row[0]->nopol . "* *Transaksi Di Batalkan* . \n \n" . "Kunjungi Halaman Berikut *" . base_url() . "* Untuk Login \n" . " Akun Anda : \n " . "Username *" . str_replace(" ", "", $row[0]->nomor_telepon) . "* \n" . "Password *" . "password" . "* \n \n";
					break;
				default:
					# code...
					break;
			}

			redirect("https://api.whatsapp.com/send?phone=" . $nomorWA . "&text=" . urlencode($pesan));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('master/Service'));
		}
	}

	public function update_action($id)
	{

		$Service = $this->Service_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('service'));
		} else {

			if (empty($Service)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('service'));
			}

			$data_service = $this->input->post();

			if ($this->input->post("status") == 5 || $this->input->post("status") == 6) {
				$data_service['tanggal_ambil'] = date('Y-m-d');
			}
			print_r($data_service);

			switch (intval($data_service["status"])) {
				case 0:
					$satus = "Penerimaan Kendaraan";
					break;
				case 1:
					$satus = "Pengecekan Kendaraan";
					break;
				case 2:
					$satus = "Penggantian Sparepart Disetujui";
					break;
				case 3:
					$satus = "Proses Pemasangan Sparepart";
					break;
				case 4:
					$satus = "Bisa Di Ambil";
					break;
				case 5:
					$satus = "Transaksi Selesai";
					break;
				case 6:
					$satus = "Transaksi Di Batalkan";
					break;
				default:
				$satus = "Penerimaan Kendaraan";
					break;
			}

			$this->Service_model->update($id, $data_service);
			$data_log["transaksi"] = $id;
			$data_log["status"] = $satus;
			$data_log["tanggal"] = date("Y-m-d H:i:s");
			$this->Service_model->insert_log($data_log);

			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('service'));
		}
	}

	public function delete_item($id)
	{
		$id_item = $this->input->get("item");
		$this->Service_model->delete_item($id_item);
		$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
		redirect(site_url('service/edit/') . $id);
	}

	public function delete($id)
	{
		$row = $this->Service_model->get_by_id($id);

		if ($row) {
			if ($row[0]->status == 5 || $row[0]->status == 2 || $row[0]->status == 3 || $row[0]->status == 4) {
				$this->session->set_flashdata('pesan', 'Data Tidak Bisa Di Hapus');
				redirect(site_url('service'));
			} else {
				$this->Service_model->delete($id);
				$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
				redirect(site_url('service'));
			}
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('service'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nama_kendaraan', 'Nama Kendaraan', 'required');
		$this->form_validation->set_rules('warna', 'Warna', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun', 'required');
		$this->form_validation->set_rules('nopol', 'Nomor Polisi', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
